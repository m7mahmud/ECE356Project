drop table if exists TempTable;

select '---------------------------------------------------------------------------------------' as '';

select 'Create TempTable' as '';

create table TempTable(
			vin char(200) default null,
            back_legroom char(25) default null,
			bedType char(25) default null,
			bedHeightInches char(25) default null,
			bedLengthInches char(25) default null, 
			bodyType char(15) default null,
            cabin char(50) default null,
            city char(15) default null,
            city_fuel_economy float(3) default null,
            combine_fuel_economy float(3) default null,
			daysOnMarket int default null,
			dealerZIP decimal(6) default null,
			description char(1) default null,
            engine_cylinders char(20) default null,
			engineDisplacement float(1) default null,
			engineType char(3) default null,
			exteriorColor char(50) default null,
            fleet char(10) default null,

			frameDamaged char(10) default null,
			franchiseDealer char(10) default null,
			franchiseMake char(30) default null,          
            front_legroom char(30) default null,

			fuelTankVolumeGal char(30) default null,
			fuelType char(30) default null,
			hasAccidents char(10) default null,
            height char(30) default null,

			highwayFuelEconomy float(2) default null,
			horsepower int default null,
			interiorColor char(50) default null,
			isCab char(10) default null,
            is_certified char(10) default null,
            is_cpo char(10) default null,

			isNew char(10) default null,
			isPreOwned char(10) default null,
			latitude float(4) default null,
			lengthInches float(1) default null,
			listingDate char(30) default null,
			listingColor char(15) default null,
			listingID decimal(10) default null,
			longitude float(4) default null,
			mainPictureURL varchar(2083) default null,
            major_options char(1) default null,

			makeName char(30) default null,
			maximumSeating int default null,
			mileage int default null,
			modelName char(30) default null,
			ownerCount int default null,
			power char(50) default null, 
			price decimal(10,2) default null,
			salvage char(10) default null,
            savings_amount int default null,

			sellerRating float(2) default null,
            sp_id int default null,
            sp_name char(50) default null,

			theftTitle char(10) default null,
            torque char(100) default null,

			transmission char(5) default null,
            transmission_display char(50) default null,
            trimId char(50) default null,

			trimName char(30) default null,
            vehicle_damage_category char(10) default null,

			wheelSystem char(10) default null,
            wheel_system_display char(50) default null,
			wheelBaseInches float(1) default null,
			widthInches float(1) default null,
			year int default null
);

-- load data local infile '/Users/muaazmahmud/Desktop/Final_ECE356Project/Data/used_cars_data.csv' ignore into table TempTable
load data infile '/var/lib/mysql-files/01-Cars/used_cars_data.csv' ignore into table TempTable
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n'
     ignore 2500000 lines;
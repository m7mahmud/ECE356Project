import mysql.connector
from datetime import datetime

# set the host to the marmoset server
# set the user to the username used to login to mysql
# set the password that you use to login to mysql
# set the name of the mysql database where the LoadDataFromCsv.sql and CreateMainTables.sql files were run
# Declaring connection parameters to database
mydb = mysql.connector.connect(
    host="marmoset04.shoshin.uwaterloo.ca", 
    user="ofdemire",
    password="Muaaz_Omer123",
    database="db356_ofdemire"
)

# Creating cursor to connect to the database
cursor = mydb.cursor(buffered=True)

# This function prints out the attributes and attribute value for a given table and id entry
def printTableAttributes(tableName, idName, idValue):
    # Creating SELECT query to obtain the entry information corresponding to parameter ID
    if (idValue == None):
        SQLQUERY = "SELECT * FROM " + str(tableName) + " LIMIT 1;"
    else:
        SQLQUERY = "SELECT * FROM " + str(tableName) + " WHERE " + str(idName) + "=" + str(idValue) + ";"
    cursor.execute(SQLQUERY)

    # Obtain result after executing query
    columns = cursor.description
    result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]

    # Print out the query
    if (idValue == None):
        for key in result[0]:
            print(str(key) + " :")
    else:
        for key in result[0]:
            print(str(key) + " : " + str(result[0][key]))


if __name__ == "__main__":

    # Creating SELECT query to obtain one item from table
    SQLQUERY = "SELECT * FROM Cars LIMIT 1;"
    cursor.execute(SQLQUERY)

    # Obtain result after executing query
    columns = cursor.description
    result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
    
    # Print out the query
    for key in result[0]:
        print(str(key) + " : " + str(result[0][key]))

    # Obtain IDs for all other tables related to listing 
    listingID = result[0]["listingID"]
    makeModelID = result[0]["makeModelID"]
    fuelID = result[0]["fuelID"]
    colorID = result[0]["colorID"]
    bedID = result[0]["bedID"]
    engineID = result[0]["engineID"]
    wheelID = result[0]["wheelID"]
    transmissionID = result[0]["transmissionID"]
    dimensionID = result[0]["dimensionID"]

    # Print attribute and attribute values corresponding to all other tables based on found IDs
    printTableAttributes("MakeModel", "makeModelID", makeModelID)
    printTableAttributes("FuelDetails", "fuelID", fuelID)
    printTableAttributes("Color", "colorID", colorID)
    printTableAttributes("BedDetails", "bedID", bedID)
    printTableAttributes("Engine", "engineID", engineID)
    printTableAttributes("Wheel", "wheelID", wheelID)
    printTableAttributes("Transmission", "transmissionID", transmissionID)
    printTableAttributes("Dimension", "dimensionID", dimensionID)
    printTableAttributes("ListingDetails", "listingID", listingID)
    printTableAttributes("PriorUsageDetails", "listingID", listingID)

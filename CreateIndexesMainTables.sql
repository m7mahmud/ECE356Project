-- Create indexes for table columns that are used by the CLI search to speed up the query
Create index Cars_Mileage on Cars(mileage);
Create index Cars_Year on Cars(year);

Create index ListingDetails_price on ListingDetails(price);

Create index MakeModel_makemodel on MakeModel(makeName, modelName);

Create index FuelDetails_fuelType on FuelDetails(fuelType);

Create index Engine_engineType on Engine(engineType);

Create index Wheel_wheelSystem on Wheel(wheelSystem);

Create index Transmission_transmission on Transmission(transmission);


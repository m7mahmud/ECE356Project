select '---------------------------------------------------------------------------------------' as '';

select 'Dropping tables' as '';

drop table if exists PriorUsageDetails;
drop table if exists ListingDetails;
drop table if exists Cars;
drop table if exists MakeModel;
drop table if exists LocationDetails;
drop table if exists FuelDetails;
drop table if exists Color;
drop table if exists BedDetails;
drop table if exists Engine;
drop table if exists Wheel;
drop table if exists Transmission;
drop table if exists Dimension;



select 'Create MakeModel' as '';

create table MakeModel(
			makeModelID int PRIMARY KEY,
			modelName char(30) NOT NULL,
			makeName char(30) NOT NULL,
			trimName char(30)
);

select 'Create FuelDetails' as '';

create table FuelDetails(
			fuelID int PRIMARY KEY,
			fuelTankVolumeGal float(2),
			fuelType char(50),
			highwayFuelEconomy float(2)
);

select 'Create Color' as '';

create table Color(
			colorID int PRIMARY KEY,
			interiorColor char(50),
			exteriorColor char(50)
);

select 'Create BedDetails' as '';

create table BedDetails(
			bedID int PRIMARY KEY,
			bedType char(20),
			bedHeightInches decimal(5,2),
			bedLengthInches decimal(5,2) 
);

select 'Create Engine' as '';

create table Engine(
			engineID int PRIMARY KEY,
			engineType char(3),
			engineDisplacement int,
			horsepower int,
			power char(50) 
);

select 'Create Wheel' as '';

create table Wheel(
			wheelID int PRIMARY KEY,
			wheelSystem char(10),
			wheelSystemDisplay char(30),
			wheelBaseInches float(1)
);

select 'Create Transmission' as '';

create table Transmission(
    transmissionID int primary key,
    transmission char(5),
	transmissionDisplay char(100)
);

select 'Create Dimension' as '';

create table Dimension(
    dimensionID int primary key,
	heightInches float(1),
    lengthInches float(1),
    widthInches float(1)
);

select 'Create Cars' as '';

create table Cars (
			listingID decimal(10) PRIMARY KEY,
			vin char(17) unique NOT NULL,
			city char(15) NOT NULL,
			listingColor char(15),
			bodyType char(15),
			maximumSeating int NOT NULL,
			mileage int,
			year int NOT NULL,
			backLegroomInches float(1),
			frontLegroomInches float(1),


			makeModelID int,
			fuelID int,
			colorID int,
			bedID int,
			engineID int,
			wheelID int,
			transmissionID int,
			dimensionID int,
			FOREIGN KEY (makeModelID) REFERENCES MakeModel(makeModelID),
			FOREIGN KEY (fuelID) REFERENCES FuelDetails(fuelID),
			FOREIGN KEY (colorID) REFERENCES Color(colorID),
			FOREIGN KEY (bedID) REFERENCES BedDetails(bedID),
			FOREIGN KEY (engineID) REFERENCES Engine(engineID),
			FOREIGN KEY (wheelID) REFERENCES Wheel(wheelID),
			FOREIGN KEY (transmissionID) REFERENCES Transmission(transmissionID),
			FOREIGN KEY (dimensionID) REFERENCES Dimension(dimensionID)
);


select 'Create ListingDetails' as '';

create table ListingDetails(
			listingID decimal(10) PRIMARY KEY,
			daysOnMarket int NOT NULL DEFAULT 0,
			listingDate date NOT NULL,
			mainPictureURL varchar(2083),
			sellerRating float(2),
			price decimal(10,2) NOT NULL,
			dealerZIP decimal(6) NOT NULL,
			franchiseDealer boolean NOT NULL,
			franchiseMake char(30),
			savingsAmount int,
			FOREIGN KEY (listingID) REFERENCES Cars(listingID)
);

select 'Create PriorUsageDetails' as '';

create table PriorUsageDetails(
			listingID decimal(10) PRIMARY KEY,
			hasAccidents boolean,
			frameDamaged boolean,
			isCab boolean,
			isNew boolean,
			isPreOwned boolean,
			ownerCount int,
			salvage boolean,
			theftTitle boolean,
			fleet boolean,
			FOREIGN KEY (listingID) REFERENCES Cars(listingID)
);
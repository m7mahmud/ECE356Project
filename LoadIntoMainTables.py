import mysql.connector
import re

# set the host to the marmoset server
# set the user to the username used to login to mysql
# set the password that you use to login to mysql
# set the name of the mysql database where the LoadDataFromCsv.sql and CreateMainTables.sql files were run
mydb = mysql.connector.connect(
host="marmoset04.shoshin.uwaterloo.ca", 
user="ofdemire",
password="Muaaz_Omer123",
database="db356_ofdemire"
)

# Connecting to the database
cursor = mydb.cursor()

# Setting global default values for the primary keys of tables
makeModelID = 1
fuelID = 1
colorID = 1
bedID = 1
engineID = 1
wheelID = 1
transmissionID = 1
dimensionID = 1

rowCounter = 0

# First runs select to check if we have already this record in Dimension Table.
# If record is already exists then this function will return the id of existing function.
# Otherwise will create a new record with provided argumnets and return the id of newly created record.
# Uses global id counters to make sure new record has a unique id.
def getOrAppendIDDimension(heightInches, lengthInches, widthInches):
    global dimensionID

    # Update the width, height, and length to remove units
    widthInches = str(widthInches)
    widthInches = re.sub("[^0-9.]", "", widthInches)

    heightInches = str(heightInches)
    heightInches = re.sub("[^0-9.]", "", heightInches)

    lengthInches = str(lengthInches)
    lengthInches = re.sub("[^0-9.]", "", lengthInches)

    # Creating SELECT query to obtain attributes from Dimension table
    sql_select_query =  "SELECT * FROM Dimension WHERE "

    # Concating the updated width, length, and height to query based on if value is NULL or not
    if(len(widthInches) == 0 or float(widthInches) == 0):
        widthInches = None
        sql_select_query = sql_select_query + " widthInches is NULL"
    else:
        sql_select_query = sql_select_query + " widthInches = " + str(widthInches)

    if(len(heightInches) == 0 or float(heightInches) == 0):
        heightInches = None
        sql_select_query = sql_select_query + " and heightInches is NULL"
    else:
        sql_select_query = sql_select_query + " and heightInches = " + str(heightInches)

    if(len(lengthInches) == 0  or float(lengthInches) == 0):
        lengthInches = None
        sql_select_query = sql_select_query + " and lengthInches is NULL"
    else:
        sql_select_query = sql_select_query + " and lengthInches = " + str(lengthInches)


    # Return -1 if all parameter table attributes are empty
    if(( heightInches == None ) and ( lengthInches == None ) and (widthInches == None)):
        return -1

    # Execute the query and obtain the results
    # print(sql_select_query)
    cursor.execute(sql_select_query)
    columns = cursor.description
    resultRows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]

    # If result exists return the dimension ID attribute
    # Else insert the a new row into the table with updated parameter attribute values
    if( len(resultRows) > 0 ):
        return int(resultRows[0]['dimensionID'])
    else:
        sql = "INSERT INTO Dimension (dimensionID, heightInches, lengthInches, widthInches) VALUES (%s ,%s ,%s , %s)"
        update_val = (dimensionID, heightInches, lengthInches, widthInches)
        cursor.execute(sql, update_val)
        mydb.commit()
        dimensionID = dimensionID + 1
        # updates the global id counter
        # print(cursor.rowcount, "record inserted.")
    
    # Return the dimension ID
    return dimensionID - 1

# First runs select to check if we have already this record in Transmission Table.
# If record is already exists then this function will return the id of existing function.
# Otherwise will create a new record with provided argumnets and return the id of newly created record.
# Uses global id counters to make sure new record has a unique id.
def getOrAppendIDTansmission(transmission, transmissionDisplay):
    global transmissionID

    # Create SELECT query to obtain all attributes from Transmission table
    sql_select_query =  "SELECT * FROM Transmission WHERE "

    # Concating the parameter attributes to query based on if empty or not parameter attribute values
    if(len(transmission) == 0 or transmission == "None"):
        transmission = None
        sql_select_query = sql_select_query + " transmission is NULL"
    else:
        sql_select_query = sql_select_query + " transmission = \"" + str(transmission) + "\""

    if(len(transmissionDisplay) == 0 or transmissionDisplay == "None"):
        transmissionDisplay = None
        sql_select_query = sql_select_query + " and transmissionDisplay is NULL"
    else:
        sql_select_query = sql_select_query + " and transmissionDisplay = \"" + str(transmissionDisplay) + "\""


    # Return -1 if all attribute parameter values are empty
    if(( transmission == None ) and ( transmissionDisplay == None )):
        return -1

    # Execute the query and obtain the results
    # print(sql_select_query)
    cursor.execute(sql_select_query)
    columns = cursor.description
    resultRows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]

    # If results exist - return transmission ID attribute value of first result
    # Else - insert into Transmission table with parameter values 
    if( len(resultRows) > 0 ):
        return int(resultRows[0]['transmissionID'])
    else:
        sql = "INSERT INTO Transmission (transmissionID, transmission, transmissionDisplay) VALUES (%s , %s , %s)"
        update_val = (transmissionID, transmission, transmissionDisplay)
        cursor.execute(sql, update_val)
        mydb.commit()
        transmissionID = transmissionID + 1
        # print(cursor.rowcount, "record inserted.")
    
    # Return transmission ID
    return transmissionID - 1


# First runs select to check if we have already this record in Wheel Table.
# If record is already exists then this function will return the id of existing function.
# Otherwise will create a new record with provided argumnets and return the id of newly created record.
# Uses global id counters to make sure new record has a unique id.
def getOrAppendIDWheel(wheelSystem, wheelSystemDisplay, wheelBaseInches):
    global wheelID

    # Update attribute to remove units from parameter value
    wheelBaseInches = str(wheelBaseInches)

    wheelBaseInches = re.sub("[^0-9.]", "", wheelBaseInches)

    # Create SELECT query to obtain all attributes from Wheel table
    sql_select_query =  "SELECT * FROM Wheel WHERE "

    # Concating to query with parameter values and formatted based on if NULL or not
    if(len(wheelBaseInches) == 0 or float(wheelBaseInches) == 0):
        wheelBaseInches = None
        sql_select_query = sql_select_query + " wheelBaseInches is NULL"
    else:
        sql_select_query = sql_select_query + " wheelBaseInches = " + str(wheelBaseInches)

    if(len(str(wheelSystem)) == 0):
        wheelSystem = None
        sql_select_query = sql_select_query + " and wheelSystem is NULL"
    else:
        sql_select_query = sql_select_query + " and wheelSystem = \"" + str(wheelSystem) + "\""

    if(len(str(wheelSystemDisplay)) == 0):
        wheelSystemDisplay = None
        sql_select_query = sql_select_query + " and wheelSystemDisplay is NULL"
    else:
        sql_select_query = sql_select_query + " and wheelSystemDisplay = \"" + str(wheelSystemDisplay) + "\""

    if(( wheelSystem == None ) and ( wheelSystemDisplay == None ) and (wheelBaseInches == None)):
        return -1

    # Execute the query and obtain the results
    # print(sql_select_query)
    cursor.execute(sql_select_query)
    columns = cursor.description
    resultRows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]

    # If results exist - return wheel ID attribute value of first result
    # Else - insert into table with updated parameter values 
    if( len(resultRows) > 0 ):
        return int(resultRows[0]['wheelID'])
    else:
        sql = "INSERT INTO Wheel (wheelID, wheelSystem, wheelSystemDisplay, wheelBaseInches) VALUES (%s ,%s ,%s , %s)"
        update_val = (wheelID, wheelSystem, wheelSystemDisplay, wheelBaseInches)
        cursor.execute(sql, update_val)
        mydb.commit()
        wheelID = wheelID + 1
        # print(cursor.rowcount, "record inserted.")
    
    # Return previous wheel ID
    return wheelID - 1

# First runs select to check if we have already this record in Dimension Table.
# If record is already exists then this function will return the id of existing function.
# Otherwise will create a new record with provided argumnets and return the id of newly created record.
# Uses global id counters to make sure new record has a unique id.
def getOrAppendIDEngine(engineType, engineDisplacement, horsepower, power):
    global engineID

    # Create SELECT query to obtain all attributes from Engine table
    sql_select_query =  "SELECT * FROM Engine WHERE "

    # Concating to the query with parameter values and formatted based on value being NULL or not
    if(len(engineType) == 0 or engineType == "None"):
        engineType = None
        sql_select_query = sql_select_query + " engineType is NULL"
    else:
        sql_select_query = sql_select_query + " engineType = \"" + str(engineType) + "\""

    if(len(str(engineDisplacement)) == 0):
        engineDisplacement = None
        sql_select_query = sql_select_query + " and engineDisplacement is NULL"
    else:
        sql_select_query = sql_select_query + " and engineDisplacement = " + str(engineDisplacement)

    if(len(str(horsepower)) == 0):
        horsepower = None
        sql_select_query = sql_select_query + " and horsepower is NULL"
    else:
        sql_select_query = sql_select_query + " and horsepower = " + str(horsepower)

    if(len(power) == 0 or power == "None"):
        power = None
        sql_select_query = sql_select_query + " and power is NULL"
    else:
        sql_select_query = sql_select_query + " and power = \"" + str(power) + "\""

    # Return -1 if parameter attribute values are all none
    if(( engineType == None ) and ( engineDisplacement == None ) and (horsepower == None) and (power == None)):
        return -1

    # Execute query and obtain results
    # print(sql_select_query)
    cursor.execute(sql_select_query)
    columns = cursor.description
    resultRows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]

    # If results exist - return engine ID attribute from first result
    # Else - insert into Engine table with parameter values
    if( len(resultRows) > 0 ):
        return int(resultRows[0]['engineID'])
    else:
        sql = "INSERT INTO Engine (engineID, engineType, engineDisplacement, horsepower, power) VALUES (%s ,%s ,%s , %s , %s)"
        update_val = (engineID, engineType, engineDisplacement, horsepower, power)
        cursor.execute(sql, update_val)
        mydb.commit()
        engineID = engineID + 1
        # print(cursor.rowcount, "record inserted.")
    
    # Return previous engine ID 
    return engineID - 1

# First runs select to check if we have already this record in Color Table.
# If record is already exists then this function will return the id of existing function.
# Otherwise will create a new record with provided argumnets and return the id of newly created record.
# Uses global id counters to make sure new record has a unique id.
def getOrAppendIDColor(interiorColor, exteriorColor):
    global colorID

    # Create SELECT query to obtain all attributes from Color table
    sql_select_query =  "SELECT * FROM Color WHERE "

    # Concating query with parameter values and formatted based on if values are NULL or not
    if(len(interiorColor) == 0 or interiorColor == "None"):
        interiorColor = None
        sql_select_query = sql_select_query + " interiorColor is NULL"
    else:
        sql_select_query = sql_select_query + " interiorColor = \"" + str(interiorColor) + "\""

    if(len(exteriorColor) == 0 or exteriorColor == "None"):
        exteriorColor = None
        sql_select_query = sql_select_query + " and exteriorColor is NULL"
    else:
        sql_select_query = sql_select_query + " and exteriorColor = \"" + str(exteriorColor) + "\""

    # Return -1 if all parameter values are empty
    if(( exteriorColor == None ) and ( interiorColor == None )):
        return -1

    # Execute query and obtain results
    # print(sql_select_query)
    cursor.execute(sql_select_query)
    columns = cursor.description
    resultRows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]

    # If results exist - return color ID attribute from first result
    # Else - insert into the Color table with parameter values
    if( len(resultRows) > 0 ):
        return int(resultRows[0]['colorID'])
    else:
        sql = "INSERT INTO Color (colorID, interiorColor, exteriorColor) VALUES (%s , %s , %s)"
        update_val = (colorID, interiorColor, exteriorColor)
        cursor.execute(sql, update_val)
        mydb.commit()
        colorID = colorID + 1
        # print(cursor.rowcount, "record inserted.")
    
    # Return previous color ID
    return colorID - 1

# First runs select to check if we have already this record in Fuel Details Table.
# If record is already exists then this function will return the id of existing function.
# Otherwise will create a new record with provided argumnets and return the id of newly created record.
# Uses global id counters to make sure new record has a unique id.
def getOrAppendIDFuelDetails(fuelTankVolumeGal, fuelType, highwayFuelEconomy):
    global fuelID
    
    # Create SELECT query to obtain all attributes from FuelDetails table
    sql_select_query =  "SELECT * FROM FuelDetails WHERE "

    # Update values to remove units
    fuelTankVolumeGal = re.sub("[^0-9.]", "", fuelTankVolumeGal)

    # Concating query with parameter values and formatted based on if value is NULL or not
    if(len(fuelTankVolumeGal) == 0 or float(fuelTankVolumeGal) == 0):
        fuelTankVolumeGal = None
        sql_select_query = sql_select_query + " fuelTankVolumeGal is NULL"
    else:
        sql_select_query = sql_select_query + " fuelTankVolumeGal = " + str(fuelTankVolumeGal)

    if(len(fuelType) == 0):
        fuelType = None
        sql_select_query = sql_select_query + " and fuelType is NULL"
    else:
        sql_select_query = sql_select_query + " and fuelType = \"" + str(fuelType) + "\""

    if(len(str(highwayFuelEconomy)) == 0):
        highwayFuelEconomy = None
        sql_select_query = sql_select_query + " and highwayFuelEconomy is NULL"
    else:
        sql_select_query = sql_select_query + " and highwayFuelEconomy = " + str(highwayFuelEconomy)

    if(( fuelTankVolumeGal == None ) and ( highwayFuelEconomy == None ) and (fuelType == None)):
        return -1
    
    # Execute query and obtain results
    # print(sql_select_query)
    cursor.execute(sql_select_query)
    columns = cursor.description
    resultRows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]

    # If results exist - return fuel ID from first result
    # Else - insert parameter values into FuelDetails table
    if( len(resultRows) > 0 ):
        return int(resultRows[0]['fuelID'])
    else:
        sql = "INSERT INTO FuelDetails (fuelID, fuelTankVolumeGal, fuelType, highwayFuelEconomy) VALUES (%s , %s , %s, %s)"
        update_val = (fuelID, fuelTankVolumeGal, fuelType, highwayFuelEconomy)
        cursor.execute(sql, update_val)
        mydb.commit()
        fuelID = fuelID + 1
        # print(cursor.rowcount, "record inserted.")
    
    # Return previous fuel ID
    return fuelID - 1

# First runs select to check if we have already this record in MakeModel Table.
# If record is already exists then this function will return the id of existing function.
# Otherwise will create a new record with provided argumnets and return the id of newly created record.
# Uses global id counters to make sure new record has a unique id.
def getOrAppendIDMakeModel(modelName, makeName, trimName):
    global makeModelID

    # Create SELECT query to obtain all the attributes from MakeModel table 
    sql_select_query =  "SELECT * FROM MakeModel WHERE modelName = %s and makeName = %s "

    # Concating query with parameter value and formatting based on value being NULL or not
    if(len(trimName) == 0):
        trimName = None
        sql_select_query = sql_select_query + "and trimName is NULL"
    else:
        sql_select_query = sql_select_query + "and trimName = \"" + str(trimName) + "\""

    # Executing query and obtaining results
    val = (modelName, makeName)
    cursor.execute(sql_select_query, val)
    columns = cursor.description
    resultRows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
    # print(resultRows)

    # If result exists - return makeModelID attribute from first result
    # Else - insert into the MakeModel table with the parameter values
    if( len(resultRows) > 0 ):
        return int(resultRows[0]['makeModelID'])
    else:
        sql = "INSERT INTO MakeModel (makeModelID, modelName, makeName, trimName) VALUES (%s , %s, %s, %s)"
        update_val = (makeModelID, modelName, makeName, trimName)
        cursor.execute(sql, update_val)
        mydb.commit()
        makeModelID = makeModelID + 1
        # print(cursor.rowcount, "record inserted.")

    # Return previous makeModel ID
    return makeModelID - 1

# First runs select to check if we have already this record in Bed Details Table.
# If record is already exists then this function will return the id of existing function.
# Otherwise will create a new record with provided argumnets and return the id of newly created record.
# Uses global id counters to make sure new record has a unique id.
def getOrAppendBedDetails(bedType, bedHeightInches, bedLengthInches):
    global bedID

    # Create SELECT query to obtain all attributes from BedDetails table
    sql_select_query =  "SELECT * FROM BedDetails WHERE "

    # Update parameter attribute values to remove units
    bedHeightInches = re.sub("[^0-9.]", "", bedHeightInches)
    bedLengthInches = re.sub("[^0-9.]", "", bedLengthInches)

    # Concating query with updated parameter values and formatted based on if value NULL or not
    if(len(bedType) == 0):
        bedType = None
        sql_select_query = sql_select_query + "bedType is NULL and "
    else:
        sql_select_query = sql_select_query + "bedType = \"" + str(bedType) + "\" and "
        
    if(len(bedHeightInches) == 0 or float(bedHeightInches) == 0):
        bedHeightInches = None
        sql_select_query = sql_select_query + "bedHeightInches is NULL and "
    else:
        sql_select_query = sql_select_query + "bedHeightInches = " + str(bedHeightInches) + " and "
    
    if(len(bedLengthInches) == 0 or float(bedLengthInches) == 0):
        bedLengthInches = None
        sql_select_query = sql_select_query + "bedLengthInches is NULL"
    else:
        sql_select_query = sql_select_query + "bedLengthInches = " + str(bedLengthInches)

    # Return -1 if all parameter values are empty
    if(( bedType == None ) and ( bedHeightInches == None ) and (bedLengthInches == None)):
        return -1

    # Execute query and obtain results
    # print(sql_select_query)
    cursor.execute(sql_select_query)
    columns = cursor.description
    resultRows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]

    # If results exist - return bed ID from first result
    # Else - insert into BedDetails table with parameter values
    if( len(resultRows) > 0 ):
        return int(resultRows[0]['bedID'])
    else:
        sql = "INSERT INTO BedDetails (bedID, bedType, bedHeightInches, bedLengthInches) VALUES (%s , %s , %s, %s)"
        update_val = (bedID, bedType, bedHeightInches, bedLengthInches)
        cursor.execute(sql, update_val)
        mydb.commit()
        bedID = bedID + 1
        # print(cursor.rowcount, "record inserted.")
    
    # Return previous bed ID
    return bedID - 1

# -1 shows that that car record doesn't have information about for that table.
def appendCars(listingID, vin, city, listingColor, bodyType, maximumSeating, mileage, year, backLegroomInches, frontLegroomInches, tempMakeModelID, tempFuelID, tempColorID, tempBedID, tempEngineID, tempWheelID, tempTransmissionID, tempDimensionID):
    # Set parameter temp IDs to None if -1
    if(tempMakeModelID == -1):
        tempMakeModelID = None
    
    if(tempFuelID == -1):
        tempFuelID = None
    
    if(tempColorID == -1):
        tempColorID = None

    if(tempBedID == -1):
        tempBedID = None

    if(tempEngineID == -1):
        tempEngineID = None

    if(tempWheelID == -1):
        tempWheelID = None

    if(tempTransmissionID == -1):
        tempTransmissionID = None

    if(tempDimensionID == -1):
        tempDimensionID = None

    # Update parameter values to remove units
    backLegroomInches = re.sub("[^0-9.]", "", backLegroomInches)
    frontLegroomInches = re.sub("[^0-9.]", "", frontLegroomInches)

    # Set parameter value to None if parameter is empty or zero
    if(len(backLegroomInches) == 0 or float(backLegroomInches) == 0):
        backLegroomInches = None

    if(len(frontLegroomInches) == 0 or float(frontLegroomInches) == 0):
        frontLegroomInches = None

    # if(type(mileage) == string.class and len(mileage) == 0):
    #     mileage = None

    if(len(bodyType) == 0):
        bodyType = None

    if(len(listingColor) == 0):
        listingColor = None

    # INSERT into Cars table with updated parameter values 
    sql = "INSERT INTO Cars (listingID, vin, city, listingColor, bodyType, maximumSeating, mileage, year, backLegroomInches, frontLegroomInches, makeModelID, fuelID, colorID, bedID, engineID, wheelID, transmissionID, dimensionID) VALUES (%s , %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    update_val = (listingID, vin, city, listingColor, bodyType, maximumSeating, mileage, year, backLegroomInches, frontLegroomInches, tempMakeModelID, tempFuelID, tempColorID, tempBedID, tempEngineID, tempWheelID, tempTransmissionID, tempDimensionID)
    cursor.execute(sql, update_val)
    mydb.commit()
    

# Listing Details table includes a lot of boolean type, so we handle boolean type data in this function
def appendListingDetails(listingID, daysOnMarket, listingDate, mainPictureURL, sellerRating, price, dealerZIP, franchiseDealer, franchiseMake, savingsAmount):
    # Set parameter values to None if value is empty
    if(len(mainPictureURL) == 0):
        mainPictureURL = None

    if(len(str(sellerRating)) == 0):
        sellerRating = None

    if(len(franchiseMake) == 0):
        franchiseMake = None

    # If the franchiseDealer parameter value is not blank, replace True/False with corresponding boolean integer
    if(len(str(franchiseDealer)) == 0):
        franchiseDealer = None
    else:
        if(franchiseDealer == "False"):
            franchiseDealer = 0
        elif(franchiseDealer == "True"):
            franchiseDealer = 1
    
    # Insert into ListingDetails table with updated parameter values
    sql = "INSERT INTO ListingDetails (listingID, daysOnMarket, listingDate, mainPictureURL, sellerRating, price, dealerZIP, franchiseDealer, franchiseMake, savingsAmount) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    update_val = (listingID, daysOnMarket, listingDate, mainPictureURL, sellerRating, price, dealerZIP, franchiseDealer, franchiseMake, savingsAmount)
    cursor.execute(sql, update_val)
    mydb.commit()



def appendPriorUsageDetails(listingID, hasAccidents, frameDamaged, isCab, isNew, isPreOwned, ownerCount, salvage, theftTitle, fleet):
    # Update parameter value to None if empty or to corresponding boolean integer based on value
    if(len(str(hasAccidents)) == 0):
        hasAccidents = None
    else:
        if(hasAccidents == "False"):
            hasAccidents = 0
        elif(hasAccidents == "True"):
            hasAccidents = 1
    
    if(len(str(fleet)) == 0):
        fleet = None
    else:
        if(fleet == "False"):
            fleet = 0
        elif(fleet == "True"):
            fleet = 1

    if(len(str(frameDamaged)) == 0):
        frameDamaged = None
    else:
        if(frameDamaged == "False"):
            frameDamaged = 0
        elif(frameDamaged == "True"):
            frameDamaged = 1

    if(len(str(isCab)) == 0):
        isCab = None
    else:
        if(isCab == "False"):
            isCab = 0
        elif(isCab == "True"):
            isCab = 1
    
    if(len(str(isNew)) == 0):
        isNew = None
    else:
        if(isNew == "False"):
            isNew = 0
        elif(isNew == "True"):
            isNew = 1

    if(len(str(isPreOwned)) == 0):
        isPreOwned = None
    else:
        if(isPreOwned == "False"):
            isPreOwned = 0
        elif(isPreOwned == "True"):
            isPreOwned = 1

    if(len(str(ownerCount)) == 0):
        ownerCount = None
    else:
        if(ownerCount == "False"):
            ownerCount = 0
        elif(ownerCount == "True"):
            ownerCount = 1

    if(len(str(salvage)) == 0):
        salvage = None
    else:
        if(salvage == "False"):
            salvage = 0
        elif(salvage == "True"):
            salvage = 1

    if(len(str(theftTitle)) == 0):
        theftTitle = None
    else:
        if(theftTitle == "False"):
            theftTitle = 0
        elif(theftTitle == "True"):
            theftTitle = 1

    # Insert into the PriorUsageDetails table with updated parameter values
    # print(hasAccidents)
    sql = "INSERT INTO PriorUsageDetails (listingID, hasAccidents, frameDamaged, isCab, isNew, isPreOwned, ownerCount, salvage, theftTitle, fleet) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    update_val = (listingID, hasAccidents, frameDamaged, isCab, isNew, isPreOwned, ownerCount, salvage, theftTitle, fleet)
    cursor.execute(sql, update_val)
    mydb.commit()


# reading the each row and split them and feed the corresponding functions to insert new records if there is a need, or return id of existing record 
def readRow(row):
    global rowCounter 

    # Obtain temporary ID value calling corresponding function with table attribute values as parameters
    print("row counter => ", rowCounter)
    tempMakeModelID = getOrAppendIDMakeModel(row["modelName"], row["makeName"], row["trimName"])
    # print("makemodelID=> " , tempMakeModelID)

    tempFuelID = getOrAppendIDFuelDetails(row["fuelTankVolumeGal"], row["fuelType"], row["highwayFuelEconomy"])
    # print("fuelID => ", tempFuelID)

    tempColorID = getOrAppendIDColor(row["interiorColor"], row["exteriorColor"])
    # print("colorID => ", tempColorID)

    tempBedID = getOrAppendBedDetails(row["bedType"], row["bedHeightInches"], row["bedLengthInches"])
    # print("bedID => ", tempBedID)

    tempEngineID= getOrAppendIDEngine(row["engineType"], row["engineDisplacement"], row["horsepower"], row["power"])
    # print("engineID => ", tempEngineID)

    tempWheelID = getOrAppendIDWheel(row["wheelSystem"], row["wheel_system_display"], row["wheelBaseInches"])
    # print("wheelID => ", tempWheelID)

    tempTransmissionID = getOrAppendIDTansmission(row["transmission"], row["transmission_display"])
    # print("transmissionID => ", tempTransmissionID)

    tempDimensionID = getOrAppendIDDimension(row["height"], row["lengthInches"], row["widthInches"])
    # print("dimensionID => ", tempDimensionID)

    appendCars(row["listingID"], row["vin"], row["city"], row["listingColor"], row["bodyType"], row["maximumSeating"], row["mileage"], row["year"], row["back_legroom"], row["front_legroom"] , tempMakeModelID, tempFuelID, tempColorID, tempBedID, tempEngineID, tempWheelID, tempTransmissionID, tempDimensionID)

    appendPriorUsageDetails(row["listingID"], row["hasAccidents"], row["frameDamaged"], row["isCab"], row["isNew"], row["isPreOwned"], row["ownerCount"], row["salvage"], row["theftTitle"], row["fleet"])

    appendListingDetails(row["listingID"], row["daysOnMarket"], row["listingDate"], row["mainPictureURL"], row["sellerRating"], row["price"], row["dealerZIP"], row["franchiseDealer"], row["franchiseMake"], row["savings_amount"])

    rowCounter = rowCounter + 1

    

if __name__ == "__main__":
    # Execute SELECT query to obtain all the attributes and data from TempTable and obtain results
    cursor.execute("SELECT * FROM TempTable")
    columns = cursor.description 
    result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]

    print('Total Row(s):', cursor.rowcount)

    # Call readRow for every row within the result
    for row in result:
        try:
            readRow(row)
        except Exception as e:
            print("Exception occured")
            print(e)

    print(mydb)


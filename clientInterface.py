import mysql.connector #run python3 -m pip install mysql-connector-python 
from tabulate import tabulate #run python3 -m pip install tabulate
from datetime import datetime

# set the host to the marmoset server
# set the user to the username used to login to mysql
# set the password that you use to login to mysql
# set the name of the mysql database where the LoadDataFromCsv.sql and CreateMainTables.sql files were run
mydb = mysql.connector.connect(
host="marmoset04.shoshin.uwaterloo.ca", 
user="ofdemire",
password="Muaaz_Omer123",
database="db356_ofdemire"
)


cursor = mydb.cursor(buffered=True)

def removeUnReferencedRows(makeModelID, fuelID, colorID, bedID, engineID, wheelID, transmissionID, dimensionID):
    #Remove unused columns after delete or update
    if(makeModelID != None):
        SQLQUERY = "SELECT listingID FROM Cars WHERE makeModelID=" + str(makeModelID) + ";"
        cursor.execute(SQLQUERY)
        if(int(cursor.rowcount) == 0):
            DeleteQuery = "Delete from MakeModel where makeModelID =" + str(makeModelID) + ";"
            cursor.execute(DeleteQuery)
            mydb.commit()

    if(fuelID != None):
        SQLQUERY = "SELECT listingID FROM Cars WHERE fuelID=" + str(fuelID) + ";"
        cursor.execute(SQLQUERY)
        if(int(cursor.rowcount) == 0):
            DeleteQuery = "Delete from FuelDetails where fuelID =" + str(fuelID) + ";"
            cursor.execute(DeleteQuery)
            mydb.commit()
    
    if(colorID != None):
        SQLQUERY = "SELECT listingID FROM Cars WHERE colorID=" + str(colorID) + ";"
        cursor.execute(SQLQUERY)
        if(int(cursor.rowcount) == 0):
            DeleteQuery = "Delete from Color where colorID =" + str(colorID) + ";"
            cursor.execute(DeleteQuery)
            mydb.commit()

    if(bedID != None):
        SQLQUERY = "SELECT listingID FROM Cars WHERE bedID=" + str(bedID) + ";"
        cursor.execute(SQLQUERY)
        if(int(cursor.rowcount) == 0):
            DeleteQuery = "Delete from BedDetails where bedID =" + str(bedID) + ";"
            cursor.execute(DeleteQuery)
            mydb.commit()

    if(engineID != None):
        SQLQUERY = "SELECT listingID FROM Cars WHERE engineID=" + str(engineID) + ";"
        cursor.execute(SQLQUERY)
        if(int(cursor.rowcount) == 0):
            DeleteQuery = "Delete from Engine where engineID =" + str(engineID) + ";"
            cursor.execute(DeleteQuery)
            mydb.commit()

    if(wheelID != None):
        SQLQUERY = "SELECT listingID FROM Cars WHERE wheelID=" + str(wheelID) + ";"
        cursor.execute(SQLQUERY)
        if(int(cursor.rowcount) == 0):
            DeleteQuery = "Delete from Wheel where wheelID =" + str(wheelID) + ";"
            cursor.execute(DeleteQuery)
            mydb.commit()

    if(transmissionID != None):
        SQLQUERY = "SELECT listingID FROM Cars WHERE transmissionID=" + str(transmissionID) + ";"
        cursor.execute(SQLQUERY)
        if(int(cursor.rowcount) == 0):
            DeleteQuery = "Delete from Transmission where transmissionID =" + str(transmissionID) + ";"
            cursor.execute(DeleteQuery)
            mydb.commit()

    if(dimensionID != None):
        SQLQUERY = "SELECT listingID FROM Cars WHERE dimensionID=" + str(dimensionID) + ";"
        cursor.execute(SQLQUERY)
        if(int(cursor.rowcount) == 0):
            DeleteQuery = "Delete from Dimension where dimensionID =" + str(dimensionID) + ";"
            cursor.execute(DeleteQuery)
            mydb.commit()

#this function is used to search for a car
def searchCar():
    #getting initial user input which will be used to filter search result
    print("You have selected to search for a vehicle for sale.")
    print("")

    print("#--------Searching For A Vehicle Sale--------#")

    #make name is required when searching
    carMake = input("Enter the make of the vehicle (required) : ")

    selectModel = input("Would you like to select a specific model? (Y/N) : ")
    carModel = None
    if (selectModel == "Y"):
        #list the model names if user chooses to specify a model name
        SQLQUERY_MAKE = "SELECT DISTINCT modelName FROM MakeModel WHERE makeName=\"" + carMake + "\" Order by modelName asc;"
        cursor.execute(SQLQUERY_MAKE)
        columns = cursor.description
        result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
        print("#--------Models For Selected Make--------#")
        for model in result:
            print(model["modelName"])

        carModel = input("Enter the model of the vehicle from the above list : ")
    
    print("")
    print("############################# START OF NOTE #################################")
    print("For the following fields perform one of the following options: ")
    print("1) Click ENTER if you do not want to specify a value for the filter.")
    print("2) Enter a value for the specificed filter.")
    print("############################## END OF NOTE ##################################")
    print("")

    #some more user input
    carMinYear = input("Minimum Year : ")
    carMaxYear = input("Maximum Year : ")
    carMinMileage = input("Minimum Mileage : ")
    carMaxMileage = input("Maximum Mileage : ")
    carMaxPrice = input("Maximum Price : ")

    #build up the where clause for the sql query that will be run 
    #only add columns that do not have a empty value specified by the user
    WhereClause = "where makeName = \"" + str(carMake)+ "\""
    if(carModel != None):
        WhereClause = WhereClause + " and modelName = \"" + str(carModel) + "\""
    if(len(str(carMinYear)) != 0):
        WhereClause = WhereClause + " and year >= " + str(carMinYear)
    if(len(str(carMaxYear)) != 0):
        WhereClause = WhereClause + " and year <= " + str(carMaxYear)
    if(len(str(carMinMileage)) != 0):
        WhereClause = WhereClause + " and mileage >= " + str(carMinMileage)
    if(len(str(carMaxMileage)) != 0):
        WhereClause = WhereClause + " and mileage <= " + str(carMaxMileage)
    if(len(str(carMaxPrice)) != 0):
        WhereClause = WhereClause + " and price <= " + str(carMaxPrice)

    #this option allows the user to specifiy more column values that they want to filter by
    filterMore = input("Would you like to set advanced filter options? (Y/N) : ")
    if (filterMore == "Y"):
        print("")
        print("############################# START OF NOTE #################################")
        print("For the following fields perform one of the following options: ")
        print("1) Click ENTER if you do not want to specify a value for the filter.")
        print("2) Enter a value for the specificed filter.")
        print("############################## END OF NOTE ##################################")
        print("")

        carMinPrice = input("Minimum Price : ")
        carBodyType = input("Body Type : ")
        carExteriorColor = input("Listing Color : ")
        carDriveTrain = input("Wheel System : ")
        carFuelType = input("Fuel Type : ")
        carEngine = input("Engine : ")
        carTransmission = input("Transmission : ")
        carSeatingCapacity = input("Seating Capacity : ")

        #add to where clause these addition column values as long as they are not empty
        if(len(str(carMinPrice)) != 0):
            WhereClause = WhereClause + " and price >= " + str(carMinPrice)
        if(len(str(carBodyType)) != 0):
            WhereClause = WhereClause + " and bodyType = \"" + str(carBodyType) + "\""
        if(len(str(carExteriorColor)) != 0):
            WhereClause = WhereClause + " and listingColor = \"" + str(carExteriorColor) + "\""
        if(len(str(carDriveTrain)) != 0):
            WhereClause = WhereClause + " and wheelSystem = \"" + str(carDriveTrain) + "\""
        if(len(str(carFuelType)) != 0):
            WhereClause = WhereClause + " and fuelType = \"" + str(carFuelType) + "\""
        if(len(str(carEngine)) != 0):
            WhereClause = WhereClause + " and engineType = \"" + str(carEngine) + "\""
        if(len(str(carTransmission)) != 0):
            WhereClause = WhereClause + " and transmission = \"" + str(carTransmission) + "\""
        if(len(str(carSeatingCapacity)) != 0):
            WhereClause = WhereClause + " and maximumSeating = \"" + str(carSeatingCapacity) + "\""

    #build up the table joins that will be used in the sql query
    #this also adds the where clause at the end of the joins
    Joins = " from Cars inner join ListingDetails using(listingID) left join PriorUsageDetails using(listingID)"
    Joins = Joins + " left join MakeModel using(makeModelID) "
    Joins = Joins + " left join FuelDetails using(fuelID) "
    Joins = Joins + " left join Color using(colorID) "
    Joins = Joins + " left join BedDetails using(bedID) "
    Joins = Joins + " left join Engine using(engineID) "
    Joins = Joins + " left join Wheel using(wheelID) "
    Joins = Joins + " left join Transmission using(transmissionID) "
    Joins = Joins + " left join Dimension using(dimensionID) "
    Joins = Joins + WhereClause


    #Specify the column values that should be returned by the query and add the join and where on to it
    #Note: in this section the columns have been split into multiple select querys so that the output result does not look messy
    SQLQuery = "Select listingID, makeName, modelName, year, mileage, price, bodyType, listingColor, wheelSystem, fuelType, engineType, transmission, maximumSeating"
    SQLQuery = SQLQuery + Joins
    try:
        #try to execute the query and print the results
        cursor.execute(SQLQuery)
        myresult = cursor.fetchall()
        field_names = [i[0] for i in cursor.description]
        print(tabulate(myresult, headers=field_names, tablefmt='grid', disable_numparse=True))
    except Exception as e:
        print("*** Search Failed. Try Again. ***")
        return

    #ask the user if they wish to see more columns
    addMore = input("Show more columns in result? (Y/N) : ")
    if(addMore != "Y"):
        return

    #Specify the column values that should be returned by the query and add the join and where on to it
    SQLQuery = "Select listingID, vin, city, backLegroomInches, frontLegroomInches, daysOnMarket, listingDate, sellerRating, dealerZip, franchiseDealer, franchiseMake"
    SQLQuery = SQLQuery + Joins
    try:
        cursor.execute(SQLQuery)
        myresult = cursor.fetchall()
        field_names = [i[0] for i in cursor.description]
        print(tabulate(myresult, headers=field_names, tablefmt='grid', disable_numparse=True))
    except Exception as e:
        print("*** Search Failed. Try Again. ***")
        return

    #ask the user if they wish to see more columns
    addMore = input("Show more columns in result? (Y/N) : ")
    if(addMore != "Y"):
        return

    #Specify the column values that should be returned by the query and add the join and where on to it
    SQLQuery = "Select listingID, savingsAmount, mainPictureURL"
    SQLQuery = SQLQuery + Joins
    try:
        cursor.execute(SQLQuery)
        myresult = cursor.fetchall()
        field_names = [i[0] for i in cursor.description]
        print(tabulate(myresult, headers=field_names, tablefmt='grid', disable_numparse=True))
    except Exception as e:
        print("*** Search Failed. Try Again. ***")
        return

    #ask the user if they wish to see more columns
    addMore = input("Show more columns in result? (Y/N) : ")
    if(addMore != "Y"):
        return

    #Specify the column values that should be returned by the query and add the join and where on to it
    SQLQuery = "Select listingID, hasAccidents, frameDamaged, isCab, isNew, isPreOwned, ownerCount, salvage, theftTitle, fleet, trimName"
    SQLQuery = SQLQuery + Joins
    try:
        cursor.execute(SQLQuery)
        myresult = cursor.fetchall()
        field_names = [i[0] for i in cursor.description]
        print(tabulate(myresult, headers=field_names, tablefmt='grid', disable_numparse=True))
    except Exception as e:
        print("*** Search Failed. Try Again. ***")
        return

    #ask the user if they wish to see more columns
    addMore = input("Show more columns in result? (Y/N) : ")
    if(addMore != "Y"):
        return

    #Specify the column values that should be returned by the query and add the join and where on to it
    SQLQuery = "Select listingID, fuelTankVolumeGal, highwayFuelEconomy, interiorColor, exteriorColor"
    SQLQuery = SQLQuery + Joins
    try:
        cursor.execute(SQLQuery)
        myresult = cursor.fetchall()
        field_names = [i[0] for i in cursor.description]
        print(tabulate(myresult, headers=field_names, tablefmt='grid', disable_numparse=True))
    except Exception as e:
        print("*** Search Failed. Try Again. ***")
        return

    #ask the user if they wish to see more columns
    addMore = input("Show more columns in result? (Y/N) : ")
    if(addMore != "Y"):
        return
    
    #Specify the column values that should be returned by the query and add the join and where on to it
    SQLQuery = "Select listingID, bedType, bedHeightInches, bedLengthInches, engineDisplacement, horsepower, power"
    SQLQuery = SQLQuery + Joins
    try:
        cursor.execute(SQLQuery)
        myresult = cursor.fetchall()
        field_names = [i[0] for i in cursor.description]
        print(tabulate(myresult, headers=field_names, tablefmt='grid', disable_numparse=True))
    except Exception as e:
        print("*** Search Failed. Try Again. ***")
        return

    #ask the user if they wish to see more columns
    addMore = input("Show more columns in result? (Y/N) : ")
    if(addMore != "Y"):
        return

    #Specify the column values that should be returned by the query and add the join and where on to it
    SQLQuery = "Select listingID, wheelSystemDisplay, wheelBaseInches, transmissionDisplay, heightInches, lengthInches, widthInches"
    SQLQuery = SQLQuery + Joins
    try:
        cursor.execute(SQLQuery)
        myresult = cursor.fetchall()
        field_names = [i[0] for i in cursor.description]
        print(tabulate(myresult, headers=field_names, tablefmt='grid', disable_numparse=True))
    except Exception as e:
        print("*** Search Failed. Try Again. ***")
        return


# Obtain input from user based on parameter string question, 
# Return None if input blank else return inputted value
def addAttribute(question):
    value = input(question)
    if (len(str(value)) == 0): 
        return None
    else:
        return value

# This function is for required input from the user
# Parameter question is used to request an input from user
# If user input is blank, continue to request an input until it is not blank
# Return user input
def requiredAttribute(question):
    value = input(question)
    while (len(str(value)) == 0):
        print("")
        print("It appears you have left this field blank. This field is required. Please enter a value.")
        value = input(question)
    
    return value

# This function checks if a row with matching attributes exists in the given table
# Return -2 if all user inputs for the attributes is blank
# Return -1 if no row matches in the parameter table
# Return ID of row that matches the parameter attributes
def checkNewExists(attributes, idName, tableName):
    allNone = True
    # Loop through all attributes, check if any are not None
    for attribute in attributes:
        if (attribute[1] != None):
            allNone = False
            break

    # Return -2 if no attributes have an input from the user
    if (allNone == True): return -2
    
    # Create a SELECT query to find a row with matching attribute values
    SQLQUERY = "SELECT " + str(idName) + " FROM " + str(tableName) + " WHERE "

    index = 0
    # Loop through all attributes and concat to SELECT query with attribute name and value
    # based on position in query, if a string/number, and if blank value
    for attribute in attributes:
        if (attribute[1] == None):
            if (index == len(attributes) - 1):
                SQLQUERY = SQLQUERY + str(attribute[0]) + " IS NULL;"
            else:
                SQLQUERY = SQLQUERY + str(attribute[0]) + " IS NULL AND "
        elif (attribute[2] == "n"):
            if (index == len(attributes) - 1):
                SQLQUERY = SQLQUERY + str(attribute[0]) + "=" + str(attribute[1]) + ";"
            else:
                SQLQUERY = SQLQUERY + str(attribute[0]) + "=" + str(attribute[1]) + " AND "
        elif (attribute[2] == "s"):
            if (index == len(attributes) - 1):
                SQLQUERY = SQLQUERY + str(attribute[0]) + "=\"" + str(attribute[1]) + "\";"
            else:
                SQLQUERY = SQLQUERY + str(attribute[0]) + "=\"" + str(attribute[1]) + "\" AND "
        index = index + 1
    
    # Execute SELECT query on mySQL server
    cursor.execute(SQLQUERY)

    # If empty set (zero rows) returned, return -1 
    if(int(cursor.rowcount) == 0):
        return -1
    else:
        # If not empty set returned, obtain and return ID value from query execute return
        columns = cursor.description
        result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
        return int(result[0][str(idName)])

# This function gets the maximum ID value with the given query and ID name
def getMaxID(queryString, idName):
    # print(queryString)

    # Execute the SELECT query string parameter
    cursor.execute(queryString)

    # Obtain and return the value from query execute return
    columns = cursor.description
    result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
    maxIDName = "MAX(" + str(idName) + ")"

    return int(result[0][str(maxIDName)])

# This function inserts data in the parameter table using parameter attributes and ID values
def insertNewVehicle(attributes, tableName, idName, idValue):
    # Create INSERT query string
    SQLQUERY = "INSERT INTO " + str(tableName) + " VALUES (" + str(idValue) + ", "

    index = 0
    # Loop through all attributes and concat attribute to INSERT query based on
    # if string/number, if empty value and position in the query
    for attribute in attributes:
        if (attribute[2] == "s"):
            if (index == len(attributes) - 1):
                if (attribute[1] == None):
                    SQLQUERY = SQLQUERY + "NULL);"
                else:
                    SQLQUERY = SQLQUERY + "\"" + str(attribute[1]) + "\");"
            else:
                if (attribute[1] == None):
                    SQLQUERY = SQLQUERY + "NULL, "
                else:
                    SQLQUERY = SQLQUERY + "\"" + str(attribute[1]) + "\", "
        elif (attribute[2] == "n"):
            if (index == len(attributes) - 1):
                if (attribute[1] == None):
                    SQLQUERY = SQLQUERY + "NULL);"
                else:
                    SQLQUERY = SQLQUERY + str(attribute[1]) + ");"
            else:
                if (attribute[1] == None):
                    SQLQUERY = SQLQUERY + "NULL, "
                else:
                    SQLQUERY = SQLQUERY + str(attribute[1]) + ", "
        index = index + 1

    # print(tableName + " : " + SQLQUERY)

    # Execute the INSERT query and save the changes in the database
    cursor.execute(SQLQUERY)
    mydb.commit()

# This function adds a car to the database based on user inputs
def addCar():
    print("You have selected to add a vehicle for sale.")
    print("")

    # Required fields note to notify user the following fields are required
    print("#################### START OF REQUIRED FIELDS - NOTE ########################")
    print("Please enter values for the following fields.")
    print("##################### END OF REQUIRED FIELDS - NOTE #########################")
    print("")

    # Requesting input from user with a prompt as parameter in requiredAttribute function
    print("#--------Adding A Vehicle Sale--------#")
    carVIN = requiredAttribute("Enter the VIN of the vehicle : ") # Cars Table

    # We search the Cars table and if vin from user input already exists in Cars table, 
    # we request for a different VIN if the user wants to, else we return to the main menu.
    SQLQUERY = "SELECT vin FROM Cars WHERE vin=\"" + str(carVIN) + "\";"
    cursor.execute(SQLQUERY)
    if (int(cursor.rowcount) == 1):
        while (int(cursor.rowcount) == 1):
            print("")
            print("A listing already exists for a vehicle with the VIN " + str(carVIN) + ".")
            print("Vehicle listing could not be created for the vehicle with the specified VIN.")
            print("")

            enterADifferentVIN = input("Would you like to enter a different VIN? (Y/N) : ")
            if (enterADifferentVIN == "Y"):
                carVIN = requiredAttribute("Enter the VIN of the vehicle : ") # Cars Table
                SQLQUERY = "SELECT vin FROM Cars WHERE vin=\"" + str(carVIN) + "\";"
                cursor.execute(SQLQUERY)
            else:
                return

    # Requesting inputs from user with a prompt as parameter in requiredAttribute function
    carMake = requiredAttribute("Enter the make of the vehicle : ") # Make Model Table
    carModel = requiredAttribute("Enter the model of the vehicle : ") # Make Model Table
    carTrim = requiredAttribute("Enter the trim of the vehicle : ") # Make Model Table
    carYear = requiredAttribute("Enter the year of the vehicle : ") # Cars Table
    carColor = requiredAttribute("Enter the color of the vehicle : ") # Cars Table
    carTransmissionType = requiredAttribute("Enter the transmission type of the vehicle (A = Automatic | M = Manual | CVT = CVT) : ") # Transmission Table
    carMileage = requiredAttribute("Enter the mileage of the vehicle (in miles) : ") # Cars Table
    carSeating = requiredAttribute("Enter maximum seating capacity of the vehicle : ") # Cars Table
    carCity = requiredAttribute("Enter the city the vehicle being listed in : ") # Cars Table
    carPrice = requiredAttribute("Enter the listing price of the vehicle : ") # Listing Details Table
    carDealerZIP = requiredAttribute("Dealer ZIP : ")
    carFranchiseDealer = requiredAttribute("Is the dealer a franchise dealer (True/False)? : ")

    # Optional fields note to notify user that following fields are optional and the options the user can input
    print("")
    print("################## START OF NOT REQUIRED FIELDS - NOTE ######################")
    print("For the following fields perform one of the following options: ")
    print("1) Click ENTER if you would do not want to specify a value for the field.")
    print("2) Enter a value for the specified field.")
    print("################### END OF NOT REQUIRED FIELDS - NOTE #######################")
    print("")

    # Array to store array of attributes
    allAttributes = []

    # For each table we create an array called attributeArr that stores
    # - attribute name
    # - attribute value
    # - if the value is a string or number (in other words requires quotes for query or not)
    # We append the array to allAttributes array to be used later 
    # We then check if row exists in table with provided attribute value inputs using checkNewExists function and store return value

    # Make Model Table
    attributeArr = [["modelName", carModel, "s"],
                    ["makeName", carMake, "s"],
                    ["trimName", carTrim, "s"]]
    allAttributes.append(attributeArr)
    makeModelTable = checkNewExists(attributeArr, "makeModelID", "MakeModel")

    # Fuel Details Table
    carFuelTankVolumeGal = addAttribute("Fuel Tank Size (in gallons) : ")
    carFuelType = addAttribute("Fuel Type : ")
    carHighwayFuelEconomy = addAttribute("Highway Fuel Economy : ")

    attributeArr = [["fuelTankVolumeGal", carFuelTankVolumeGal, "n"],
                    ["fuelType", carFuelType, "s"],
                    ["highwayFuelEconomy", carHighwayFuelEconomy, "n"]]
    allAttributes.append(attributeArr)
    fuelDetailsTable = checkNewExists(attributeArr, "fuelID", "FuelDetails")

    # Color Table
    carInteriorColor = addAttribute("Interior Color : ")
    carExteriorColor = addAttribute("Exterior Color (specific name of color) : ")

    attributeArr = [["interiorColor", carInteriorColor, "s"], 
                    ["exteriorColor", carExteriorColor, "s"]]
    allAttributes.append(attributeArr)
    colorTable = checkNewExists(attributeArr, "colorID", "Color")

    # Bed Details Table
    carBedType = addAttribute("Bed Type : ")
    carBedHeightInches = addAttribute("Bed Height (in inches) : ")
    carBedLengthInches = addAttribute("Bed Length (in inches) : ")

    attributeArr = [["bedType", carBedType, "s"],
                    ["bedHeightInches", carBedHeightInches, "n"],
                    ["bedLengthInches", carBedLengthInches, "n"]]
    allAttributes.append(attributeArr)
    bedDetailsTable = checkNewExists(attributeArr, "bedID", "BedDetails")

    # Engine Table
    carEngineType = addAttribute("Engine Type (such as V6/V4/I4) : ")
    carEngineDisplacement = addAttribute("Engine Displacement : ")
    carHorsepower = addAttribute("Horsepower : ")
    carPower = addAttribute("Power (HP @ RPM) : ")

    attributeArr = [["engineType", carEngineType, "s"],
                    ["engineDisplacement", carEngineDisplacement, "n"],
                    ["horsepower", carHorsepower, "n"],
                    ["power", carPower, "s"]]
    allAttributes.append(attributeArr)
    engineTable = checkNewExists(attributeArr, "engineID", "Engine")

    # Wheel Table
    carWheelSystem = addAttribute("Wheel System (such as FWD/RWD) : ")
    carWheelSystemDisplay = addAttribute("Wheel System Display (such as Front-Wheel-Drive/Rear-Wheel-Drive) : ")
    carWheelBaseInches = addAttribute("Wheel Base (in inches) : ")

    attributeArr = [["wheelSystem", carWheelSystem, "s"],
                    ["wheelSystemDisplay", carWheelSystemDisplay, "s"],
                    ["wheelBaseInches", carWheelBaseInches, "n"]]
    allAttributes.append(attributeArr)
    wheelTable = checkNewExists(attributeArr, "wheelID", "Wheel")

    # Transmission Table
    carTransmissionDisplay = addAttribute("Transmission Display (such as A/M/CVT) : ")

    attributeArr = [["transmission", carTransmissionType, "s"],
                    ["transmissionDisplay", carTransmissionDisplay, "s"]]
    allAttributes.append(attributeArr)
    transmissionTable = checkNewExists(attributeArr, "transmissionID", "Transmission")

    # Dimension Table 
    carHeightInches = addAttribute("Height (in inches) : ")
    carLengthInches = addAttribute("Length (in inches) : ")
    carWidthInches = addAttribute("Width (in inches) : ")

    attributeArr = [["heightInches", carHeightInches, "n"],
                    ["lengthInches", carLengthInches, "n"],
                    ["widthInches", carWidthInches, "n"]]
    allAttributes.append(attributeArr)
    dimensionTable = checkNewExists(attributeArr, "dimensionID", "Dimension")

    # Cars Table
    carBodyType = addAttribute("Body Type (such as SUV/Sedan) : ")
    carBackLegroomInches = addAttribute("Back Legroom (in inches) : ")
    carFrontLegroomInches = addAttribute("Front Legroom (in inches) : ")

    attributeArr = [["vin", carVIN, "s"],
                    ["city", carCity, "s"],
                    ["listingColor", carColor, "s"],
                    ["bodyType", carBodyType, "s"],
                    ["maximumSeating", carSeating, "n"],
                    ["mileage", carMileage, "n"],
                    ["year", carYear, "n"],
                    ["backLegroomInches", carBackLegroomInches, "n"],
                    ["frontLegroomInches", carFrontLegroomInches, "n"]]
    allAttributes.append(attributeArr)
    carsTable = checkNewExists(attributeArr, "listingID", "Cars")

    # Listing Details Table
    carDaysOnMarket = 0
    carListingDate = addAttribute("Listing Date (YYYY/MM/DD) : ")

    # If user input for listing date was empty, use the current date formatted as YYYY/MM/DD
    if (carListingDate == None):
        carListingDate = datetime.today().strftime("%Y/%m/%d")

    carMainPictureURL = addAttribute("Main Picture URL : ")
    carSellerRating = addAttribute("Seller Rating (out of 5) : ")
    carFranchiseMake = addAttribute("Franchise Make (such as Fiat/Land Rover) : ")
    carSavingsAmount = addAttribute("Savings Amount (in USD) : ")

    attributeArr = [["daysOnMarket", carDaysOnMarket, "n"],
                    ["listingDate", carListingDate, "s"],
                    ["mainPictureURL", carMainPictureURL, "s"],
                    ["sellerRating", carSellerRating, "n"],
                    ["price", carPrice, "n"],
                    ["dealerZIP", carDealerZIP, "n"],
                    ["franchiseDealer", carFranchiseDealer, "n"],
                    ["franchiseMake", carFranchiseMake, "s"],
                    ["savingsAmount", carSavingsAmount, "n"]]
    allAttributes.append(attributeArr)
    listingDetailsTable = checkNewExists(attributeArr, "listingID", "ListingDetails")

    # PriorUsageDetails Table
    carHasAccidents = addAttribute("Has accidents (True/False)? : ")
    carFrameDamaged = addAttribute("Has frame been damaged (True/False)? : ")
    carIsCab = addAttribute("Is car cab (True/False)? : ")
    carIsNew = addAttribute("Is car new (True/False)? : ")
    carIsPreOwned = addAttribute("Is car preOwned (True/False)? : ")
    carOwnerCount = addAttribute("Owner Count : ")
    carSalvage = addAttribute("Is car salvage (True/False)? : ")
    carTheftTitle = addAttribute("Has car been stolen before (True/False)? : ")
    carFleet = addAttribute("Is car part of a fleet (True/False)? : ")

    attributeArr = [["hasAccidents", carHasAccidents, "n"],
                    ["frameDamaged", carFrameDamaged, "n"],
                    ["isCab", carIsCab, "n"],
                    ["isNew", carIsNew, "n"],
                    ["isPreOwned", carIsPreOwned, "n"],
                    ["ownerCount", carOwnerCount, "n"],
                    ["salvage", carSalvage, "n"],
                    ["theftTitle", carTheftTitle, "n"],
                    ["fleet", carFleet, "n"]]
    allAttributes.append(attributeArr)
    priorUsageDetailsTable = checkNewExists(attributeArr, "listingID", "PriorUsageDetails")

    # For each table we check if the
    # - table does not contain row with same attribute values (equal to -1)
    #   - obtain maximum table ID from the table
    #   - increment maximum table ID by 1
    #   - insert user input values and table ID into the table
    # - table contains row with same attibute values (greater than -1)
    #   - set table ID to the ID of the row with matching attribute values
    # - user input values for the table are all empty (equal to -2)
    #   - set table ID to NULL as no row exists with the same user input attribute values

    makeModelID = 0
    if(int(makeModelTable) == -1):
        SQLQUERY = "SELECT MAX(makeModelID) FROM MakeModel;"
        makeModelID = getMaxID(SQLQUERY, "makeModelID")

        makeModelID = makeModelID + 1
        insertNewVehicle(allAttributes[0], "MakeModel", "makeModelID", makeModelID)
    elif(int(makeModelTable) > -1):
        makeModelID = makeModelTable
    else:
        makeModelID = "NULL"

    fuelID = 0
    if(int(fuelDetailsTable) == -1):
        SQLQUERY = "SELECT MAX(fuelID) FROM FuelDetails;"
        fuelID = getMaxID(SQLQUERY, "fuelID")

        fuelID = fuelID + 1
        insertNewVehicle(allAttributes[1], "FuelDetails", "fuelID", fuelID)
    elif(int(fuelDetailsTable) > -1):
        fuelID = fuelDetailsTable
    else:
        fuelID = "NULL"

    colorID = 0
    if(int(colorTable) == -1):
        SQLQUERY = "SELECT MAX(colorID) FROM Color;"
        colorID = getMaxID(SQLQUERY, "colorID")

        colorID = colorID + 1
        insertNewVehicle(allAttributes[2], "Color", "colorID", colorID)
    elif(int(colorTable) > -1):
        colorID = colorTable
    else:
        colorID = "NULL"

    bedID = 0
    if(int(bedDetailsTable) == -1):
        SQLQUERY = "SELECT MAX(bedID) FROM BedDetails;"
        bedID = getMaxID(SQLQUERY, "bedID")

        bedID = bedID + 1
        insertNewVehicle(allAttributes[3], "BedDetails", "bedID", bedID)
    elif(int(bedDetailsTable) > -1):
        bedID = bedDetailsTable
    else:
        bedID = "NULL"

    engineID = 0
    if(int(engineTable) == -1):
        SQLQUERY = "SELECT MAX(engineID) FROM Engine;"
        engineID = getMaxID(SQLQUERY, "engineID")

        engineID = engineID + 1
        insertNewVehicle(allAttributes[4], "Engine", "engineID", engineID)
    elif(int(engineTable) > -1):
        engineID = engineTable
    else:
        engineID = "NULL"

    wheelID = 0
    if(int(wheelTable) == -1):
        SQLQUERY = "SELECT MAX(wheelID) FROM Wheel;"
        wheelID = getMaxID(SQLQUERY, "wheelID")

        wheelID = wheelID + 1
        insertNewVehicle(allAttributes[5], "Wheel", "wheelID", wheelID)
    elif(int(wheelTable) > -1):
        wheelID = wheelTable
    else:
        wheelID = "NULL"

    transmissionID = 0
    if(int(transmissionTable) == -1):
        SQLQUERY = "SELECT MAX(transmissionID) FROM Transmission;"
        transmissionID = getMaxID(SQLQUERY, "transmissionID")

        transmissionID = transmissionID + 1
        insertNewVehicle(allAttributes[6], "Transmission", "transmissionID", transmissionID)
    elif(int(transmissionTable) > -1):
        transmissionID = transmissionTable
    else:
        transmissionID = "NULL"

    dimensionID = 0
    if(int(dimensionTable) == -1):
        SQLQUERY = "SELECT MAX(dimensionID) FROM Dimension;"
        dimensionID = getMaxID(SQLQUERY, "dimensionID")

        dimensionID = dimensionID + 1
        insertNewVehicle(allAttributes[7], "Dimension", "dimensionID", dimensionID)
    elif(int(dimensionTable) > -1):
        dimensionID = dimensionTable
    else:
        dimensionID = "NULL"

    # Append all the found IDs from above to the array for Cars table
    allAttributes[8].append(["makeModelID", makeModelID, "n"])
    allAttributes[8].append(["fuelID", fuelID, "n"])
    allAttributes[8].append(["colorID", colorID, "n"])
    allAttributes[8].append(["bedID", bedID, "n"])
    allAttributes[8].append(["engineID", engineID, "n"])
    allAttributes[8].append(["wheelID", wheelID, "n"])
    allAttributes[8].append(["transmissionID", transmissionID, "n"])
    allAttributes[8].append(["dimensionID", dimensionID, "n"])

    # Initizalize listing ID variable
    listingID = 0

    # Obtain maximum listing ID from Cars table
    SQLQUERY = "SELECT MAX(listingID) FROM Cars;"
    listingID = getMaxID(SQLQUERY, "listingID")

    # Increment maximum listing ID by 1 for the new listing and insert attributes and new 
    # listing ID for the Cars, ListingDetails, and PriorUsageDetails tables
    listingID = listingID + 1
    insertNewVehicle(allAttributes[8], "Cars", "listingID", listingID)
    insertNewVehicle(allAttributes[9], "ListingDetails", "listingID", listingID)
    insertNewVehicle(allAttributes[10], "PriorUsageDetails", "listingID", listingID)
    print("Listing has been added with listing ID: ", listingID)

#this function is used to remove a listing/Car
def deleteCar():
    print("You have selected to remove a vehicle for sale.")
    print("")

    #get the listingID that the user wants to remove
    carListingID = input("Enter listing ID of the vehicle : ")

    #search for listingID in database
    SQLQUERY_LISTINGID = "SELECT * FROM Cars WHERE listingID=" + carListingID + ";"
    cursor.execute(SQLQUERY_LISTINGID)
    columns = cursor.description
    result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]

    #After running the query check that the row count is not 0. If it is 0 it means that the listing does not exist
    if (int(cursor.rowcount) == 0):
        tryAgain = input("Listing could not be found. Would you like to search again? (Y/N) : ")
        if (tryAgain == "Y"):
            deleteCar()
        else:
            return
    else:
        # The listingID exists so drop the listing for the given listingID
        for row in result:
            makeModelID = row["makeModelID"]
            fuelID = row["fuelID"]
            colorID = row["colorID"]
            bedID = row["bedID"]
            engineID = row["engineID"]
            wheelID = row["wheelID"]
            transmissionID = row["transmissionID"]
            dimensionID = row["dimensionID"]

            #Delete the listing from the ListingDetails table
            SQLDeleteQuery = "Delete from ListingDetails where listingID = " + carListingID + ";"
            # print(SQLDeleteQuery)
            cursor.execute(SQLDeleteQuery)
            mydb.commit()
            #Delete the listing from the PriorUsageDetails table
            SQLDeleteQuery = "Delete from PriorUsageDetails where listingID = " + carListingID + ";"
            cursor.execute(SQLDeleteQuery)
            mydb.commit()
            #Delete the listing from the Cars table
            SQLDeleteQuery = "Delete from Cars where listingID = " + carListingID + ";"
            cursor.execute(SQLDeleteQuery)
            mydb.commit()

            #search through the MakeModel, FuelDetails, Color, BedDetails, Engine, Wheel, Transmission, and Dimension tables 
            #and remove the row if it is no longer referenced. For example a makeModelID may be used by multiple listings
            #so we should not remove it. But if a makeModelID is no longer being referenced by any listing then remove it.
            removeUnReferencedRows(makeModelID, fuelID, colorID, bedID, engineID, wheelID, transmissionID, dimensionID)

        print("Listing has been deleted.")

def executeUpdate(attributeDict, tableName, IDname, ID):
    # runs update query to change given table using provided id value

    if(len(attributeDict) > 0 ):
        SQLQUERY = "Update "+ tableName + " set "

        for key in attributeDict:
            SQLQUERY = SQLQUERY + key + " = " + str(attributeDict[key]) + " , "

        SQLQUERY = SQLQUERY[:len(SQLQUERY) - 2]

        SQLQUERY = SQLQUERY + " where "+ IDname +" = " + str(ID)
        cursor.execute(SQLQUERY)
        mydb.commit()
        # prints the feedback of successful record after update operation
        print("Records are successfully updated")


def insertNewRecord(attributeDict, tableName, idValue):
    # Once user ask for an update and if the existing tables doesn't have that record at all, we dont update, we insert a new record
    # After this new record is created with a new id, we use this id to match with the record in Cars table

    if(len(attributeDict) > 0 ):
        SQLQUERY = "insert into " + str(tableName)

        SQLQUERY = SQLQUERY + " ( "

        for key in attributeDict:
            SQLQUERY = SQLQUERY + key + " , "


        SQLQUERY = SQLQUERY[:len(SQLQUERY) - 2] + " ) "
         
        SQLQUERY = SQLQUERY + " values ("
        for key in attributeDict:
            SQLQUERY = SQLQUERY + str(attributeDict[key]) + " , "

        SQLQUERY = SQLQUERY[:len(SQLQUERY) - 2] + ")"

        cursor.execute(SQLQUERY)
        mydb.commit()

def modifyAttributeWithValue(cName, attributeDict, value):
    # To check whether we have the record of asked record by user, we fill missing attributes.
    # Missing attributes are the ones that user didn't enter any input
    value = str(value)
    valueToSave = ""
    if(len(value) == 0):
        valueToSave =  "NULL"
    else:
        if(value.isdigit()):
            valueToSave = int(value)

        elif(value.replace(".", "", 1).isdigit()):
            valueToSave =  float(value)

        else:
            valueToSave = "\"" + value + "\""

    attributeDict[cName] =  valueToSave

def modifyAttribute(question ,cName, attributeDict):
    # asks the question, and recives input from the user then records them to our dictionary after a small data modification
    value = input(question)
    valueToSave = ""
    if(len(value) != 0):

        if(value == "empty"):
            valueToSave =  "NULL"
        elif(value == "True"):
            valueToSave =  "1"
        elif(value == "False"):
            valueToSave = "0"
        else:
            if(value.isdigit()):
                valueToSave = int(value)

            elif(value.replace(".", "", 1).isdigit()):
                valueToSave =  float(value)

            else:
                valueToSave = "\"" + value + "\""

        attributeDict[cName] =  valueToSave

def modifyAttributeCannotEmpty(question ,cName, attributeDict):
    # Same as modifyAttribute function except, it keeps prompting the question until not empty result is received
    # This is for attributes that cannot be null
    value = input(question)
    valueToSave = ""
    while(value == "empty"):
        value = input(question)
    if(len(value) != 0):
        
        if(value == "True"):
            valueToSave =  "1"
        elif(value == "False"):
            valueToSave = "0"
        else:
            if(value.isdigit()):
                valueToSave = int(value)

            elif(value.replace(".", "", 1).isdigit()):
                valueToSave =  float(value)

            else:
                valueToSave = "\"" + value + "\""

        attributeDict[cName] =  valueToSave


def updateCarsTable(listingID):
    # List of questions to update Cars table, each question corresponding to an attribute
    attributeDict = {}

    q = "Enter the VIN of the vehicle ( cannot be empty ) : "
    modifyAttributeCannotEmpty(q, "vin", attributeDict)

    q = "Enter the year of the vehicle ( cannot be empty ) : "
    modifyAttributeCannotEmpty(q, "year", attributeDict)

    q = "Enter the color of the vehicle : "
    modifyAttribute(q, "listingColor", attributeDict)

    q = "Enter the mileage of the vehicle (in miles) : "
    modifyAttribute(q, "mileage", attributeDict)

    q = "Enter maximum seating capacity of the vehicle (cannot be empty) : "
    modifyAttributeCannotEmpty(q, "maximumSeating", attributeDict)

    q = "Enter the city the vehicle being listed in : "
    modifyAttribute(q, "city", attributeDict)

    q = "Body Type (such as SUV/Sedan) : "
    modifyAttribute(q, "bodyType", attributeDict)
    
    q = "Back Legroom (in inches) : "
    modifyAttribute(q, "backLegroomInches", attributeDict)

    q = "Front Legroom (in inches) : "
    modifyAttribute(q, "frontLegroomInches", attributeDict)

    executeUpdate(attributeDict, "Cars", "listingID" , listingID)



def updateListingDetailsTable(listingID):
    # List of questions to update ListingDetails table, each question corresponding to an attribute
    attributeDict = {}
    
    q = "Listing Price (cannot be empty) : "
    modifyAttributeCannotEmpty(q, "price", attributeDict)

    q = "Car Days On Market (cannot be empty) : "
    modifyAttributeCannotEmpty(q, "daysOnMarket", attributeDict)

    q = "Listing Date (YYYY/MM/DD) (cannot be empty) : "
    modifyAttributeCannotEmpty(q, "listingDate", attributeDict)

    q = "Main Picture URL : "
    modifyAttribute(q, "mainPictureURL", attributeDict)

    q = "Seller Rating (out of 5) : "
    modifyAttribute(q, "sellerRating", attributeDict)

    q = "Dealer ZIP (cannot be empty) : "
    modifyAttributeCannotEmpty(q, "dealerZIP", attributeDict)

    q = "Is the dealer a franchise dealer (True/False)? (cannot be empty) : "
    modifyAttributeCannotEmpty(q, "franchiseDealer", attributeDict)

    q = "Franchise Make (such as Fiat/Land Rover) : "
    modifyAttribute(q, "franchiseMake", attributeDict)

    q = "Savings Amount (in USD) : "
    modifyAttribute(q, "savingsAmount", attributeDict)

    executeUpdate(attributeDict, "ListingDetails", "listingID" ,listingID)


def updatePriorUsageDetailsTable(listingID):
    # List of questions to update PriorUsageDetails table, each question corresponding to an attribute
    attributeDict = {}

    q = "Has accidents (True/False)? : "
    modifyAttribute(q, "hasAccidents", attributeDict)

    q = "Has frame been damaged (True/False)? : "
    modifyAttribute(q, "frameDamaged", attributeDict)

    q = "Is car cab (True/False)? : "
    modifyAttribute(q, "isCab", attributeDict)

    q = "Is car new (True/False)? : "
    modifyAttribute(q, "isNew", attributeDict)

    q = "Is car preOwned (True/False)? : "
    modifyAttribute(q, "isPreOwned", attributeDict)

    q = "Owner Count : "
    modifyAttribute(q, "ownerCount", attributeDict)

    q = "Is car salvage (True/False)? : "
    modifyAttribute(q, "salvage", attributeDict)

    q = "Has car been stolen before (True/False)? : "
    modifyAttribute(q, "theftTitle", attributeDict)

    q = "Is car part of a fleet (True/False)? : "
    modifyAttribute(q, "fleet", attributeDict)

    executeUpdate(attributeDict, "PriorUsageDetails", "listingID" , listingID)


def getIDOrAppend(attributeDict, tableName, IDname):
    # returns id of existing record if that matches with what user asked for
    # if there is no existing record that matches with what user asked for 
    # so we insert a new record using id which existing last id + 1

    SQLQUERY =  "select " + IDname + " from " + tableName + " where "

    for key in attributeDict:
        if(attributeDict[key] == "NULL"):
            SQLQUERY = SQLQUERY + key + " is " + str(attributeDict[key]) + " and "
        else:
            SQLQUERY = SQLQUERY + key + " = " + str(attributeDict[key]) + " and "


    SQLQUERY = SQLQUERY[:len(SQLQUERY) - 4]

    cursor.execute(SQLQUERY)
    columns = cursor.description
    resultRows = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]

    if( len(resultRows) > 0 ):
        return int(resultRows[0][IDname])
    else:
        #append newly created record
        SQLQUERY = "select MAX(" + IDname +") from "+ tableName
        newID = getMaxID(SQLQUERY, IDname)
        newID = newID + 1
        attributeDict[IDname] = newID
        insertNewRecord(attributeDict, tableName, newID)

        return newID


def getExistingRows(tableName, idName, idValue):
    # returns the records for the rest of the attributes that user didn't provide an input

    if(idValue != None and idValue != "None"):
        SQLQUERY_LISTINGID = "SELECT * FROM "+ tableName +" WHERE "+ idName + "=" + idValue
        cursor.execute(SQLQUERY_LISTINGID)
        columns = cursor.description
        result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
    else:
        SQLQUERY_LISTINGID = "SELECT * FROM "+ tableName +" LIMIT 1"
        cursor.execute(SQLQUERY_LISTINGID)
        columns = cursor.description

        result = [{}]

        for column in columns:
            result[0][column[0]] = ""

    return result


def updateMakeModelTable(row):
    # List of questions to update MakeModel table, each question corresponding to an attribute
    attributeDict = {}

    q = "Enter the make of the vehicle (cannot be empty) : "
    modifyAttributeCannotEmpty(q, "makeName", attributeDict)

    q = "Enter the model of the vehicle (cannot be empty) : "
    modifyAttributeCannotEmpty(q, "modelName", attributeDict)

    q = "Enter the trim of the vehicle : "
    modifyAttribute(q, "trimName", attributeDict)
    

    if(len(attributeDict) > 0):

        # Adds the rest of the attributes that user didn't provide any input  

        result = getExistingRows("MakeModel", "makeModelID", str(row["makeModelID"]))

        if "makeName" not in attributeDict:
            modifyAttributeWithValue("makeName", attributeDict ,result[0]["makeName"])

        if "modelName" not in attributeDict:
            modifyAttributeWithValue("modelName",attributeDict ,result[0]["modelName"])

        if "trimName" not in attributeDict:
            modifyAttributeWithValue("trimName",attributeDict, result[0]["trimName"])

        # Finally updates the cars table to match with the found id
        ID = getIDOrAppend(attributeDict, "MakeModel", "makeModelID")
        newDict = {}
        newDict["makeModelID"] = ID
        executeUpdate(newDict, "Cars", "listingID" , str(row["listingID"]))

def updateFuelDetailsTable(row):
    # List of questions to update FuelDetails table, each question corresponding to an attribute
    attributeDict = {}

    q = "Fuel Tank Size (in gallons) : "
    modifyAttribute(q, "fuelTankVolumeGal", attributeDict)

    q = "Fuel Type : "
    modifyAttribute(q, "fuelType", attributeDict)

    q = "Highway Fuel Economy : "
    modifyAttribute(q, "highwayFuelEconomy", attributeDict)

    if(len(attributeDict) > 0):

        # Adds the rest of the attributes that user didn't provide any input 

        result = getExistingRows("FuelDetails", "fuelID", str(row["fuelID"]))

        if "fuelTankVolumeGal" not in attributeDict:
            modifyAttributeWithValue("fuelTankVolumeGal", attributeDict, result[0]["fuelTankVolumeGal"])

        if "fuelType" not in attributeDict:
            modifyAttributeWithValue("fuelType", attributeDict, result[0]["fuelType"])

        if "highwayFuelEconomy" not in attributeDict:
            modifyAttributeWithValue("highwayFuelEconomy", attributeDict, result[0]["highwayFuelEconomy"])

        # Finally updates the cars table to match with the found id
        ID = getIDOrAppend(attributeDict, "FuelDetails", "fuelID")
        newDict = {}
        newDict["fuelID"] = ID
        executeUpdate(newDict, "Cars", "listingID" , str(row["listingID"]))


def updateColorTable(row):
    attributeDict = {}


    q = "Interior Color : "
    modifyAttribute(q, "interiorColor", attributeDict)

    q = "Exterior Color (specific name of color) : "
    modifyAttribute(q, "exteriorColor", attributeDict)

    if(len(attributeDict) > 0):
        # Adds the rest of the attributes that user didn't provide any input 
        result = getExistingRows("Color", "colorID", str(row["colorID"]))

        if "interiorColor" not in attributeDict:
            modifyAttributeWithValue("interiorColor", attributeDict, result[0]["interiorColor"])

        if "exteriorColor" not in attributeDict:
            modifyAttributeWithValue("exteriorColor", attributeDict, result[0]["exteriorColor"])

        # Finally updates the cars table to match with the found id

        ID = getIDOrAppend(attributeDict, "Color", "colorID")
        newDict = {}
        newDict["colorID"] = ID
        executeUpdate(newDict, "Cars", "listingID" , str(row["listingID"]))


def updateBedDetailsTable(row):
    attributeDict = {}

    q = "Bed Type : "
    modifyAttribute(q, "bedType", attributeDict)

    q = "Bed Height (in inches) : "
    modifyAttribute(q, "bedHeightInches", attributeDict)

    q = "Bed Length (in inches) : "
    modifyAttribute(q, "bedLengthInches", attributeDict)

    if(len(attributeDict) > 0):
        # Adds the rest of the attributes that user didn't provide any input 

        result = getExistingRows("BedDetails", "bedID", str(row["bedID"]))

        if "bedType" not in attributeDict:
            modifyAttributeWithValue("bedType", attributeDict, result[0]["bedType"])

        if "bedHeightInches" not in attributeDict:
            modifyAttributeWithValue("bedHeightInches", attributeDict, result[0]["bedHeightInches"])

        if "bedLengthInches" not in attributeDict:
            modifyAttributeWithValue("bedLengthInches", attributeDict, result[0]["bedLengthInches"])

        # Finally updates the cars table to match with the found id
        ID = getIDOrAppend(attributeDict, "BedDetails", "bedID")
        newDict = {}
        newDict["bedID"] = ID
        executeUpdate(newDict, "Cars", "listingID" , str(row["listingID"]))


def updateEngineTable(row):
    attributeDict = {}

    q = "Engine Type (such as V6/V4/I4) : "
    modifyAttribute(q, "engineType", attributeDict)

    q = "Engine Displacement : "
    modifyAttribute(q, "engineDisplacement", attributeDict)

    q = "Horsepower : "
    modifyAttribute(q, "horsepower", attributeDict)

    q = "Power (HP @ RPM) : "
    modifyAttribute(q, "power", attributeDict)

    if(len(attributeDict) > 0):
        # Adds the rest of the attributes that user didn't provide any input 

        result = getExistingRows("Engine", "engineID", str(row["engineID"]))

        if "engineType" not in attributeDict:
            modifyAttributeWithValue("engineType", attributeDict, result[0]["engineType"])

        if "engineDisplacement" not in attributeDict:
            modifyAttributeWithValue("engineDisplacement", attributeDict, result[0]["engineDisplacement"])

        if "horsepower" not in attributeDict:
            modifyAttributeWithValue("horsepower", attributeDict, result[0]["horsepower"])

        if "power" not in attributeDict:
            modifyAttributeWithValue( "power", attributeDict, result[0]["power"])

        # Finally updates the cars table to match with the found id
        ID = getIDOrAppend(attributeDict, "Engine", "engineID")
        newDict = {}
        newDict["engineID"] = ID
        executeUpdate(newDict, "Cars", "listingID" , str(row["listingID"]))



def updateWheelTable(row):
    attributeDict = {}

    q = "Wheel System (such as FWD/RWD) : "
    modifyAttribute(q, "wheelSystem", attributeDict)

    q = "Wheel System Display (such as Front-Wheel-Drive/Rear-Wheel-Drive) : "
    modifyAttribute(q, "wheelSystemDisplay", attributeDict)

    q = "Wheel Base (in inches) : "
    modifyAttribute(q, "wheelBaseInches", attributeDict)


    if(len(attributeDict) > 0):
        # Adds the rest of the attributes that user didn't provide any input 

        result = getExistingRows("Wheel", "wheelID", str(row["wheelID"]))

        if "wheelSystem" not in attributeDict:
            modifyAttributeWithValue("wheelSystem", attributeDict, result[0]["wheelSystem"])

        if "wheelSystemDisplay" not in attributeDict:
            modifyAttributeWithValue("wheelSystemDisplay", attributeDict, result[0]["wheelSystemDisplay"])

        if "wheelBaseInches" not in attributeDict:
            modifyAttributeWithValue("wheelBaseInches", attributeDict, result[0]["wheelBaseInches"])


        # Finally updates the cars table to match with the found id
        ID = getIDOrAppend(attributeDict, "Wheel", "wheelID")
        newDict = {}
        newDict["wheelID"] = ID
        executeUpdate(newDict, "Cars", "listingID" , str(row["listingID"]))


def updateTransmissionTable(row):
    attributeDict = {}

    q = "Transmission Display (such as A/M/CVT) : "
    modifyAttribute(q, "transmissionDisplay", attributeDict)

    q = "Enter the transmission type of the vehicle (A = Automatic | M = Manual | CVT = CVT) : "
    modifyAttribute(q, "transmission", attributeDict)


    if(len(attributeDict) > 0):
        # Adds the rest of the attributes that user didn't provide any input 

        result = getExistingRows("Transmission", "transmissionID", str(row["transmissionID"]))

        if "transmissionDisplay" not in attributeDict:
            modifyAttributeWithValue("transmissionDisplay", attributeDict, result[0]["transmissionDisplay"])

        if "transmission" not in attributeDict:
            modifyAttributeWithValue("transmission", attributeDict, result[0]["transmission"])

        # Finally updates the cars table to match with the found id
        ID = getIDOrAppend(attributeDict, "Transmission", "transmissionID")
        newDict = {}
        newDict["transmissionID"] = ID
        executeUpdate(newDict, "Cars", "listingID" , str(row["listingID"]))


def updateDimensionTable(row):
    attributeDict = {}

    q = "Height (in inches) : "
    modifyAttribute(q, "heightInches", attributeDict)

    q = "Length (in inches) : "
    modifyAttribute(q, "lengthInches", attributeDict)

    q = "Width (in inches) : "
    modifyAttribute(q, "widthInches", attributeDict)


    if(len(attributeDict) > 0):
        # Adds the rest of the attributes that user didn't provide any input 

        result = getExistingRows("Dimension", "dimensionID", str(row["dimensionID"]))

        if "heightInches" not in attributeDict:
            modifyAttributeWithValue("heightInches", attributeDict, result[0]["heightInches"])

        if "lengthInches" not in attributeDict:
            modifyAttributeWithValue("lengthInches", attributeDict, result[0]["lengthInches"])

        if "widthInches" not in attributeDict:
            modifyAttributeWithValue("widthInches", attributeDict, result[0]["widthInches"])

        # Finally updates the cars table to match with the found id
        ID = getIDOrAppend(attributeDict, "Dimension", "dimensionID")
        newDict = {}
        newDict["dimensionID"] = ID
        executeUpdate(newDict, "Cars", "listingID" , str(row["listingID"]))


##########################################################################33


def updateCar():
    print("You have selected to update a vehicle for sale.")
    print("")

    carListingID = input("Enter listing ID of the vehicle : ")
    if(carListingID.isdigit()):
        SQLQUERY_LISTINGID = "SELECT * FROM Cars WHERE listingID=" + carListingID + ";"
        cursor.execute(SQLQUERY_LISTINGID)
        columns = cursor.description
        result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]

    if (not carListingID.isdigit() or int(cursor.rowcount) == 0):
        tryAgain = input("Listing could not be found. Would you like to search again? (Y/N) : ")
        if (tryAgain == "Y"):
            updateCar()
        else:
            return
    else:
        # Update the listing for the given listing ID
        # listing ID unique, so cannot have more than one result
        row = result[0]
        print("Editing Listing, only answer questions that want to be updated")
        print("Note: press enter without typing anything to skip a column")
        print("Note: type empty if you want to update a columns value empty")

        print("#--------Updating A Vehicle Sale--------#")

        # Cars Table
        updateCarsTable(str(row["listingID"]))

        # Listing Details Table
        updateListingDetailsTable(str(row["listingID"]))

        # PriorUsageDetails Table
        updatePriorUsageDetailsTable(str(row["listingID"]))

        # MakeModel Table
        updateMakeModelTable(row)

        # Fuel Details Table
        updateFuelDetailsTable(row)

        # Color Table
        updateColorTable(row)

        # Bed Details Table
        updateBedDetailsTable(row)

        # Engine Table
        updateEngineTable(row)

        # Wheel Table
        updateWheelTable(row)

        # Transmission Table
        updateTransmissionTable(row)

        # Dimension Table 
        updateDimensionTable(row)

        removeUnReferencedRows(row["makeModelID"], row["fuelID"], row["colorID"], row["bedID"], row["engineID"], row["wheelID"], row["transmissionID"], row["dimensionID"])
        print("Listing has been updated.")

if __name__ == "__main__":

    #this allows a user to choose operations such as search, add, update and delete
    while True:
        print("#---------------------------------------#")
        print("Command List:")
        print("Searching for a vehicle for sale : s")
        print("Adding for a vehicle for sale : a")
        print("Removing a vehicle for sale : r")
        print("Update a vehicle for sale : u")
        print("Exit : e")
        print("#---------------------------------------#")
        print("")

        clientFunction = input("Enter Command : ")
        print("")

        if (clientFunction == "s"):
            searchCar()
        elif (clientFunction == "a"):
            addCar()
        elif (clientFunction == "r"):
            deleteCar()
        elif (clientFunction == "u"):
            updateCar()
        elif (clientFunction == "e"):
            break
        else:
            print("You have entered an invalid command. Please select a command from the following list.")
